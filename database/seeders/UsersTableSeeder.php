<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'firstname' => 'Meraj',
            'lastname' => 'Ahamad',
            'mobile' => '9987675645',
            'age' => '20',
            'gender' => 'm',
            'email' => 'john@doe.com',
            'password' => Hash::make('password')
        ]);
    }
}
