<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    function index(Request $request){

     
        $userData=User::all();
 

        return $userData;


    }


    function login(Request $request)
    {

        
        
        $user= User::where('email', $request->email)->first();
       
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'message' => ['These credentials do not match our records.']
                ], 404);
            }
        
             $token = $user->createToken('my-app-token')->plainTextToken;
             
            $response = [
                'code_status' => '200',
                'token' => $token
            ];
        
             return response($response, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $rule= array("firstname" => "required",
            "lastname" => "required",
            "mobile" => "required|numeric",
            "age" => "required",
            "gender" =>"required",
            "email" => "required|email",
            "password"=>"required");
       
       $validator=Validator::make($request->all(),$rule);
       if($validator->fails())
       {
            return $validator->errors();
       }else{
        $password=Hash::make($request->password);
        $user=new User;

        $user->firstname=$request->firstname;
        $user->lastname=$request->lastname;
        $user->mobile=$request->mobile;
        $user->age=$request->age;
        $user->gender=$request->gender;
        $user->email=$request->email;
        $user->password=$password;
        if($user->save()){

            $response = [
                'code_status' => '200',
                'message' => 'User Registerd successfully'
            ];
        
             return response($response, 201);

        }else{

            $response = [
                
                'message' => 'Not registerd something wrong'
            ];
        
             return response($response, 201);

        }
           
       }
  

   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $userdatasingle=User::find($id);
         return $userdatasingle;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $rule= array("firstname" => "required",
            "lastname" => "required",
            "mobile" => "required|numeric",
            "age" => "required",
            "gender" =>"required",
            
            );
       
       $validator=Validator::make($request->all(),$rule);
       if($validator->fails())
       {
            return $validator->errors();
       }else{
        $password=Hash::make($request->password);
        $user=User::find($id);

        $user->firstname=$request->firstname;
        $user->lastname=$request->lastname;
        $user->mobile=$request->mobile;
        $user->age=$request->age;
        $user->gender=$request->gender;
        $user->email=$request->email;
        $user->password=$password;
        if($user->save()){

            $response = [
                'code_status' => '200',
                'message' => 'User Updated successfully'
            ];
        
             return response($response, 201);

        }else{

            echo "Ohh not updated successfully";

        }
           
       }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=User::find($id);
        if($user->delete()){
            
            $response = [
                'code_status' => '200',
                'message' => 'User Deleted successfully'
            ];
        
             return response($response, 201);
        }else{
          
            $response = [
            
                'message' => 'User Not Deleted'
            ];
        
             return response($response, 400);

        }

    }

    public function logout($id)
    {
        //
    }
}
